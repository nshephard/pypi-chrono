# pypi-chrono

Query [pypi](https://pypi.org) for available package versions on a specified date.

## Background

A tweet thread from [@walkingrandomly](https://twitter.com/walkingrandomly) (Mike Croucher) who was trying to reproduce
some old work undertaking ""digital archeology"" in Python trying to reproduce some results from old code ended with the
[question](https://twitter.com/walkingrandomly/status/1546236610089467906)

> What I'm wondering is 'Is there an automated way of doing this?' That is, assume you have code that once worked. It uses a bunch of packages and have no idea what versions were used. How to quickly zero in on a possible environment?

This sparked the seed for this package as I figured it should be relatively easy to query [PyPI](https://pypi.org/) to
get the release dates of packages and tally these against a user specified date and in turn work out what packages were
used in a body of code.

## Installation

### PyPI

```bash
pip install pypi-chrono
```

### Latest

```bash
pip install git+https://gitlab.com/nshephard/pypi-chrono.git
```

## Usage

**TODO***

## Contributing

**TODO**

## Links

* [It is easier to gather package meta-data from PyPI package ecosystem, once know the right way | by R4444 | Medium](https://medium.com/@ruturajkvaidya/it-is-easier-to-gather-package-meta-data-from-pypi-package-ecosystem-once-know-the-right-way-8d21ebf3f719)

"""Tests for fetch module."""
# from datetime import datetime
import pytest

from pypi_chrono.fetch import extract_release_date, extract_releases

PACKAGE = "tcx2gpx"


def test_get_metadata(tcx2gpx_metadata) -> None:
    """Test getting metadata."""

    assert isinstance(tcx2gpx_metadata, dict)
    assert tcx2gpx_metadata["info"]["author"] == "Neil Shephard"
    assert tcx2gpx_metadata["info"]["author_email"] == "nshephard@gmail.com"
    assert tcx2gpx_metadata["info"]["name"] == "tcx2gpx"


def test_extract_release(tcx2gpx_metadata) -> None:
    """Test extracting releases"""
    releases = extract_releases(tcx2gpx_metadata)

    assert isinstance(releases, list)
    assert releases[:3] == ["0.1.2", "0.1.3", "0.1.4"]


@pytest.mark.parametrize(
    "version, package_type, target_date",
    [("0.1.2", "tar.gz", "2020-01-19T17:10:52.651594Z"), ("0.1.3", "whl", "2020-01-30T07:35:07.637389Z")],
)
def test_extract_release_date(tcx2gpx_metadata, version, package_type, target_date) -> None:
    """Test extracting release date."""
    date = extract_release_date(tcx2gpx_metadata, version, package_type)
    assert isinstance(date, str)
    assert date == target_date


# def test_date_to_datetime() -> None:
#     """Test converting date to datetime"""
#     # date = date_to_datetime("2020-01-30T07:35:09.432106Z")
#     # assert isinstance(datetime)
#     assert True


# def test_find_version() -> None:
#     """Test extracting version."""
#     assert True


# def test_delta_date() -> None:
#     """Test delta_date differences."""
#     assert True


# def test_get_version_for_date() -> None:
#     """Test extracting version for a given date."""
#     assert True

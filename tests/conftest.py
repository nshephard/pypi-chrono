"""Fixtures for tests."""
import pytest

from pypi_chrono.fetch import get_metadata

PACKAGE = "tcx2gpx"


@pytest.fixture
def tcx2gpx_metadata() -> get_metadata:
    """Fixture for package tcx2gpx metadata."""
    return get_metadata(PACKAGE)

"""Fetch data"""
from datetime import datetime
from typing import Union

import requests


def get_metadata(package: str, url: str = "https://pypi.org/pypi/") -> dict:
    """Query PyPI JSON API for package metadata.

    Parameters
    ----------
    package: str
        Package name.
    url: str
        URL of Python Package Index to query.

    Returns
    -------
    dict
        Dictionary of metadata for package.
    """
    return requests.get(url + package + "/json").json()


def extract_releases(metadata: dict) -> dict:
    """Extract releases from metadata of a package.

    Parameters
    ----------
    metadata: dict
        Metadata for a package as returned by PyPI JSON API.

    Returns
    -------
    list
        List of release dates.
    """
    return list(metadata["releases"].keys())


def extract_release_date(metadata: dict, version: str, package_type: str = ".tar.gz") -> datetime:
    """Extract release date as ISO.

    Parameters
    ----------
    metadata: dict
        Metadata for a package as returned by PyPI JSON API.
    version: str
        Version to extract date for.
    package_type: str
        Extract the release date/time for 'whl' or 'tar.gz' (default).

    Returns
    -------
    datetime:
        Release date in ISO8601
    """
    pkg_type = 0 if package_type == "whl" else 1
    return metadata["releases"][version][pkg_type]["upload_time_iso_8601"]


def date_to_datetime(date: str, fmt: str) -> datetime.timestamp:
    """Convert an ISO8601 date/time to datetime object.

    Parameters
    ----------
    date: str
        Date to be converted.
    fmt: str
        Format for string. See

    Returns
    -------
    datetime.timestamp
        Datetime object.
    """
    return datetime.strptime(date)


# def find_version(release_dates: dict, date: datetime = datetime.now()) -> str:
#     """Find the version of a package available for a given date.

#     Parameters
#     ----------
#     release_dates : dict
#         Dictionary of release dates (key) and versions (value).
#     date : datetime
#         Date on which version is required.

#     Returns
#     -------
#     str
#         The version for that date.

#     Links
#     -----

#     https://stackoverflow.com/questions/17249220/getting-the-closest-date-to-a-given-date/17249470#17249470
#     """

#     def func(x):
#         delta = x - date if x > date else timedelta.max

#     return min(
#         release_dates,
#     )
# def find_version(release_dates: dict, base_date: datetime = datetime.now()):
#     # http://stackoverflow.com/a/17249470/846892
#     # b_d = datetime.strptime(base_date, "%m/%d %I:%M %p") #Already have b_d as datetime.strptime()
#     def func(x):
#         d = datetime.strptime(x[0], "%m/%d %I:%M %p")
#         delta = d - base_date if d > base_date else timedelta.max
#         return delta

#     return min(release_dates, key=func)


# def delta_date(date: datetime, release_dates: Union[list, dict]) -> datetime:
#     """Determine"""


def get_version_for_date(package: str, date: Union[str, datetime], package_type: str = "tar.gz") -> str:
    """Get the version of a package for a specific date.

    Parameters
    ----------
    package: str
        Package name as listed on PyPi.
    date: Union[str, datetime]
        Date the available version is to be returned for.
    package_type: str
        Extract the release date/time for 'whl' or 'tar.gz' (default).

    Returns
    -------
    str
        Stable package version available on PyPI for the given date.
    """
    metadata = get_metadata(package)
    releases = extract_releases(metadata)
    dates = {extract_release_date(metadata, version, package_type): version for version in releases}
    # print(date)
    return dates


get_version_for_date(package="tcx2gpx", date="2020-02-02", package_type="tar.gz")
